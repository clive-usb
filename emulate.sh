#!/bin/bash
qemu-system-x86_64 \
-m 8G -cpu host -accel kvm -smp 8 \
-vga virtio -display gtk,gl=on,show-cursor=on \
-usb -device usb-tablet \
-audiodev driver=jack,id=audio,in.frequency=48000,out.frequency=48000 \
-device ich9-intel-hda -device hda-duplex,audiodev=audio \
-drive format=raw,file=clive-workshop.img
# \
#-device AC97,audiodev=audio \
#-audiodev driver=pa,id=audio,server=/run/user/1000/pulse/native,in.frequency=48000,out.frequency=48000,in.buffer-length=200000,out.buffer-length=200000,in.latency=100000,out.latency=100000 \
