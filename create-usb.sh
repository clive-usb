#!/bin/bash
set -vex
ISO="${1}"
DEVICE="${2}"
ENCRYPT="none"
IMAGE="clive-workshop.img"
if [ "x$(dirname "${DEVICE}")" = "x/dev" ]
then
  echo "THIS WILL DELETE ALL DATA ON ${DEVICE}"
  echo "BE SURE THIS IS THE CORRECT DEVICE"
  echo -n "Are you sure?  Type yes to continue: "
  read SURE
  if [ "xyes" != "x${SURE}" ]
  then
    exit
  fi
  LOOP=
  PART="${DEVICE}3"
else
  if [ -f "${IMAGE}" ]
  then
    echo "THIS WILL DELETE ALL DATA IN ${IMAGE}"
    echo "BE SURE THIS IS THE CORRECT FILE"
    echo -n "Are you sure?  Type yes to continue: "
    read SURE
    if [ "xyes" != "x${SURE}" ]
    then
      exit
    fi
    rm -f "${IMAGE}"
  fi
  qemu-img create -f raw "${IMAGE}" 4040724480
  LOOP="$(losetup --partscan --show --find "${IMAGE}")"
  DEVICE="${LOOP}"
  PART="${DEVICE}p3"
fi
dd if="${ISO}" of="${DEVICE}" bs=4M status=progress
sync
sleep 10
partprobe "${DEVICE}" || echo "warning ignored"
sleep 10
(
  echo 'n'
  sleep 1
  echo 'p'
  sleep 1
  echo '3'
  sleep 1
  echo ''
  sleep 1
  echo ''
  sleep 1
  echo 'w'
  sleep 10
) |
fdisk "${DEVICE}"
sleep 10
partprobe "${DEVICE}" || echo "warning ignored"
sleep 10
if [ "xluks" = "x${ENCRYPT}" ]
then
  echo "Choose a pass-phrase for encryption"
  cryptsetup --verify-passphrase luksFormat --pbkdf-memory 262144 "${PART}"
  cryptsetup luksOpen "${PART}" persist
  dd if=/dev/zero of=/dev/mapper/persist bs=4M status=progress || echo "error ignored"
  mkfs.ext4 -L persistence /dev/mapper/persist
  mkdir -p clive-workshop
  mount /dev/mapper/persist clive-workshop
  echo "/ union" > clive-workshop/persistence.conf
  sync
  umount clive-workshop
  cryptsetup luksClose persist
else
  mkfs.ext4 -L persistence "${PART}"
  mkdir -p clive-workshop
  mount "${PART}" clive-workshop
  echo "/ union" > clive-workshop/persistence.conf
  sync
  umount clive-workshop
fi
if [ "x${DEVICE}" = "x${LOOP}" ]
then
  losetup --detach "${LOOP}"
fi
