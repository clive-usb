# clive-usb

A bootable Debian Live for live-coding audio in C using Clive.

<https://mathr.co.uk/clive#usb>

## Features

- non-free firmware for wide hardware support
- persistence partition to save data between sessions
- XFCE4 desktop
- Pipewire audio system
- prerequisites for Clive

## Requirements

- amd64 CPU
- 1GB RAM

## Using

- Insert portable USB flash drive into computer.
- Turn on computer.
- You may need to change boot device order in BIOS or EFI settings.
- At USB's boot menu just press return.
- Wait until the desktop loads; login is automatic.
- If screen-saver locks desktop, username is `user`, password is `live`.

## Creating

To write the ISO to a portable USB flash drive (minimum 2GB):

```
git clone https://code.mathr.co.uk/clive-usb.git
cd clive-usb
sudo ./create-usb.sh /path/to/live-image-amd64.hybrid.iso /dev/sdX
```

MAKE SURE TO USE THE CORRECT DEVICE INSTEAD OF `/dev/sdX`
PICKING THE WRONG DRIVE CAN WIPE OUT YOUR DATA

## Building

On Debian Bullseye with live-build and cryptsetup installed:

```
git clone https://code.mathr.co.uk/clive-usb.git
cd clive-usb
./rebuild.sh
```

Building needs about 10GB of space.

## Testing

Create a 4GB USB key image and an 8GB hard drive image.

```
qemu-img create -f raw hda.img 8G
sudo ./create-usb.sh live-image-amd64.hybrid.iso
sudo chown myuser:mygroup clive-workshop.img
./emulate.sh
```

If the screen is blank in XFCE4, try switching to a
virtual terminal in the QEMU monitor:

```
sendkey ctrl-alt-f1
```

and run

```
xfconf-query -c xfwm4 -p /general/vblank_mode -s xpresent
sudo service lightdm restart
```
